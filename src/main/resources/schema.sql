create table goods (
    id serial primary key,
    description varchar(20),
    price integer,
    count integer
);

insert into goods (description, price, count) values ('Шоколад','60',6);
insert into goods (description, price, count) values ('Сок','150',3);
insert into goods (description, price, count) values ('Шины','25000',4);
insert into goods (description, price, count) values ('Шампанское','2000',4);


create table customers (
    id serial primary key,
    first_name varchar(10),
    last_name varchar(15)
);

insert into customers (first_name, last_name) values ('Петров','Петр');
insert into customers (first_name, last_name) values ('Иванов','Иван');
insert into customers (first_name, last_name) values ('Константинов','Константин');

create table orders (
    id serial primary key,
    goods_id integer,
    foreign key(goods_id) references goods(id),
    customer_id integer,
    foreign key(customer_id) references customers(id),
    data_orders date,
    count integer
);

insert into orders (goods_id, customer_id, data_orders, count) values (1,1,'2021-11-30',90);
insert into orders (goods_id, customer_id, data_orders, count) values (2,1,'2021-12-01',20);
insert into orders (goods_id, customer_id, data_orders, count) values (4,2,'2021-11-01',40);

-- количество товаров проданных за 30.11
select (select description from goods where goods_id=goods.id), SUM(count)
from orders
where data_orders='2021-11-30'
group by goods_id;

-- все продажи в руб. по всем товарам, которые есть на складе
select description, SUM(o.count*g.price)
from goods g
    left join orders o on g.id = o.goods_id
group by description

--отчет об остатках
select description, SUM(count)
from goods
group by description
